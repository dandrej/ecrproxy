/**
 * Created by Andrew on 14.05.2015.
 */
var net     = require('net');
var nconf   = require('nconf');
var express = require('express');
var bodyParser = require('body-parser');
var path    = require('path');
var http = require('http');
var winston = require('winston');
var app = express();
var io = 0;

var PRX_NAME  = 1;
var PRX_OPEN  = 2;
var PRX_CLOSE = 3;
var PRX_DATA  = 4;
var PRX_CNL   = 5;
var PRX_OPENFAIL = 6;

nconf.argv().env().file({ file: './config.json' });
nconf.defaults({
    'masterPort':6969,
    'slavePort':9000,
    'webPort':8080
});

var PORT = nconf.get('masterPort');//6969;
var HTTPPORT = nconf.get('webPort');//8080
var PORTOFFS = nconf.get('slavePort');//9000;

var log = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            filename: 'public/ecrproxy.log',
            //handleExceptions: true,
            level:'info',
            maxsize:50000,
            maxFiles:20,
            json:false
        })
    ]
});

log.on('logging', function (transport, level) {
    //console.log('Log transport',transport, level);
    if (transport.name!="file") return;
    if ((level=='info') || (level=='error')) io.emit('log');
});


// ==================================================================================================

app.use(bodyParser.json());
//app.use(express.methodOverride()); // ��������� put � delete
//app.use(app.router); // ������ ��� �������� ������� ������������ �����
app.use(express.static(path.join(__dirname, "public"))); // ������ ������������ ��������� �������, ������� ������� �� ����� public/ (� ����� ������ ������ index.html)
app.use("/js/vendor",express.static(path.join(__dirname, "bower_components/jquery/dist")));
app.use("/js/vendor",express.static(path.join(__dirname, "bower_components/underscore")));
app.use("/js/vendor",express.static(path.join(__dirname, "bower_components/backbone")));
app.use("/js/vendor",express.static(path.join(__dirname, "bower_components/backgrid/lib")));
app.use("/css",express.static(path.join(__dirname, "bower_components/backgrid/lib")));
app.use("/css",express.static(path.join(__dirname, "bower_components/bootstrap/dist/css")));
app.use("/js/vendor",express.static(path.join(__dirname, "bower_components/bootstrap/dist/js")));
app.use("/fonts",express.static(path.join(__dirname, "bower_components/bootstrap/dist/fonts")));
app.use("/js/vendor",express.static(path.join(__dirname, "node_modules/socket.io/node_modules/socket.io-client")));

//var ecrs_debug = [];
//var ecr_no=1;

app.get('/api/ecrs', function (req, res) {
    log.debug('query ecrs');
    res.set({'Content-Type':'application/json'});
    var ret = [];
    devices.forEach(function(sock,port){
        ret.push({"name":sock.name,"port":port, "started":sock.started.getTime()});
    });
    res.json(ret);
    //ecrs_debug.push({'name':"ECR_"+ecr_no,'port':9000+ecr_no,'started':new Date().getTime()});
    //ecr_no+=1;
    //res.json([{"name":"ECR1","port":9000,"started":new Date().getTime()-100000},{"name":"ECR_2","port":9001,"started":new Date().getTime()-500000}]);
    //res.json(ecrs_debug);
});

/*app.get('/api/deb', function (req, res) {
 log.debug('debug qry');
 res.set({'Content-Type':'application/json'});
 io.emit('change');
 res.json(ecrs_debug);
 });*/

var svr = http.createServer(app);
io = require('socket.io')(svr);

svr.listen(HTTPPORT, function(){
    log.info('Web server starts listening on %d',HTTPPORT);
});

// =========================================================================================================

var portFree = PORTOFFS;
var devices = new Map();

function newPort(sock) {
    log.debug('addPort');
    var port = portFree;
    while (devices.has[port]) port++;
    sock.started = new Date();
    devices.set(port,sock);
    io.emit('change');
    portFree = port+1;
    return port;
}

function delPort(port) {
    log.debug("delPort");
    devices.delete(port);
    io.emit('change');
    if (portFree>port) portFree=port;
}

function prx_data_send(no, cnl, data) {
    log.debug("send",data);
    var len = data.length;
    var offs = 0;
    while(len) {
        var thunk = (len<254)?len:254;
        cnl.write(new Buffer([PRX_DATA,thunk+1,no]));
        cnl.write(data.slice(offs,offs+thunk));
        offs+=thunk;
        len-=thunk;
    }
}

function svrHandler(sock) { // this - slave server
    this.opened.push(sock);
    log.debug('connections',this.master.svr.slaves.size,this.master.svr.maxActive);
    if (this.master.svr.slaves.size<this.master.svr.maxActive) {
        log.debug('query open connection');
        this.master.write(new Buffer([PRX_OPEN]));
    }
    sock.setKeepAlive(true,10000);
    sock.on('data', function(data) { // this - slave socket
        if (this.cnl) {
            //this.master.write(new Buffer([PRX_DATA,data.length+1,this.cnl]));
            //this.master.write(data);
            prx_data_send(this.cnl,this.master,data);
            log.debug("send",data.length+1,"bytes data");
        } else {
            if (this.data) { this.data = Buffer.concat([this.data,data]);
            } else { this.data = data;
            }
        }
    });
    sock.on('end', function(){ // this - slave socket
        if (this.cnl) {
            this.master.write(new Buffer([PRX_CLOSE,this.cnl]));
        }
    });
    sock.on('error',function(e){
        log.error('Error in slave connection',e);
    });
    sock.on('close', function() { // this - slave socket
        log.debug('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
        if (this.server.slaves && this.server.slaves.has(this.cnl)) {
            this.server.slaves.delete(this.cnl);
            if (this.server.opened.length!=0) this.master.write(new Buffer([PRX_OPEN]));
        }
    });
}

function masterCmd() { // this - master socket
    switch (this.cmd) {
        case PRX_NAME: {
            this.svr.maxActive = this.param[0];
            this.name = this.param.slice(1).toString('utf8').trim();
            if (this.name.charAt(this.name.length-1)=='\u0000') this.name = this.name.slice(0,this.name.length-1);
            log.debug('maxActive',this.svr.maxActive,'name',this.name);
            log.info('Client from %s has name %s',this.remoteAddress,this.name);
        } break;
        case PRX_CLOSE: {
            var peer = this.svr.slaves.get(this.param[0]);
            if (peer) peer.end();
        } break;
        case PRX_DATA: {
            var peer = this.svr.slaves.get(this.param[0]);
            if (peer) { peer.write(this.param.slice(1))
            }
        } break;
        case PRX_CNL: {
            var cnlno = this.param[0];
            if (this.svr.opened.length==0) break;
            var cnl = this.svr.opened.pop();
            cnl.cnl = cnlno;
            cnl.master = this;
            if (cnl.data) {
                //cnl.master.write(new Buffer([PRX_DATA,cnl.data.length+1,cnlno]));
                //cnl.master.write(cnl.data); // send more than 255 bytes at once
                prx_data_send(cnlno,cnl.master,cnl.data);
                log.debug("send"+(cnl.data.length+1)+"bytes wait data");
                delete cnl.data;
            }
            this.svr.slaves.set(cnlno,cnl);
        } break;
        case PRX_OPENFAIL: {
            log.debug("PRX_OPENFAIL");
            //if (this.svr.opened.length==0) break;
            //this.svr.opened.pop();
        } break;
    }
    delete this.param;
    delete this.cmd;
    this.cmd=0;
}

function masterData(data) { // this - master socket
    //log.debug('masterData',data);
    if (this.cmd && !this.tail) {
        this.tail=data[0];
        if (this.tail==0) masterCmd.call(this);
        data = data.slice(1);
    }
    if (this.tail) {
        var ln = (this.tail>data.length)?data.length:this.tail;
        //log.debug('copy buffer',this.param.length,this.tail,this.param.length-this.tail,ln);
        data.copy(this.param,this.param.length-this.tail,0,ln);
        data = data.slice(ln);
        this.tail-=ln;
    }
    if (this.param && (this.tail==0)) masterCmd.call(this);
    if (data.length) {
        this.cmd = data[0];
        log.debug('command',data[0]);
        if (data.length==1) return;
        this.tail = data[1];
        log.debug('length',data[1]);
        if (this.tail==0) { masterCmd.call(this);
        } else this.param = new Buffer(this.tail);
        if (data.length==2) return;
        masterData.call(this,data.slice(2));
    }
}

function endConn() {
    log.info('Close connection with %s',this.remoteAddress);
    this.svr.close();
    if (this.svr.opened) {
        while(this.svr.opened.length) this.svr.opened.pop().end();
        delete this.svr.opened;
    }
    if (this.svr.slaves) {
        this.svr.slaves.forEach(function(sock){
            if (sock.cnl) delete sock.cnl;
            sock.end();
        });
        delete this.svr.slaves;
    }
    delPort(this.svr.listenPort);
    log.debug('port closed');
}

net.createServer(function(master) {
    log.debug('CONNECTED: ' + master.remoteAddress +':'+ master.remotePort);

    master.svr = net.createServer(svrHandler);
    master.svr.master = master;
    var port = newPort(master);
    master.svr.listenPort = port;
    master.svr.opened = [];
    master.svr.slaves = new Map();
    master.svr.listen(port);
    log.info('Client from %s mapped to %d',master.remoteAddress,port);

    master.setKeepAlive(true,10000);
    master.on('data', masterData);
    master.on('end', function(){ // this - master socket
        endConn.call(this);
    });
    master.on('error',function(e){
        log.error('Socket error on master socket %s',this.remoteAddress,e);
        endConn.call(this);
    });
    master.on('close', function() { // this - master socket
        //log.debug('CLOSED: ' + master.remoteAddress +' '+ master.remotePort);
    });
}).listen(PORT);

log.info('Socket server starts listening on %d',PORT);

