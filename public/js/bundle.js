(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

var Ecr = (function (_Backbone$Model) {
    _inherits(Ecr, _Backbone$Model);

    function Ecr() {
        _classCallCheck(this, Ecr);

        _get(Object.getPrototypeOf(Ecr.prototype), 'constructor', this).apply(this, arguments);
    }

    _createClass(Ecr, [{
        key: 'parse',
        value: function parse(resp, opt) {
            _.extend(resp, { 'url': { 'title': resp.name, 'ref': window.location.protocol + '//' + window.location.hostname + ':' + resp.port } });
            return resp;
        }
    }]);

    return Ecr;
})(Backbone.Model);

var Ecrs = Backbone.Collection.extend({
    model: Ecr,
    'url': '/api/ecrs'
});

var UrlCell = (function (_Backgrid$UriCell) {
    _inherits(UrlCell, _Backgrid$UriCell);

    function UrlCell() {
        _classCallCheck(this, UrlCell);

        _get(Object.getPrototypeOf(UrlCell.prototype), 'constructor', this).apply(this, arguments);
    }

    _createClass(UrlCell, [{
        key: 'render',
        value: function render() {
            this.$el.empty();
            var rawValue = this.model.get(this.column.get('name'));
            if (_.isObject(rawValue)) {
                this.title = rawValue.title || this.title;
                rawValue = rawValue.ref;
            }
            var formattedValue = this.formatter.fromRaw(rawValue, this.model);
            this.$el.append($('<a>', {
                tabIndex: -1,
                href: rawValue,
                title: this.title || formattedValue,
                target: this.target
            }).text(this.title || formattedValue));
            this.delegateEvents();
            return this;
        }
    }]);

    return UrlCell;
})(Backgrid.UriCell);

var columns = [{
    'name': 'url',
    'label': 'Каса',
    'cell': UrlCell,
    'editable': false
}, {
    'name': 'started',
    'label': 'З`єднано',
    'cell': 'datetime',
    'editable': false
}];

var ecrs = new Ecrs();

var grid = new Backgrid.Grid({
    columns: columns,
    collection: ecrs,
    className: 'backgrid table table-bordered'
});

function getLog() {
    $.get('ecrproxy.log').done(function (data) {
        $('#logtxt').html(data.replace(/\r\n/, '<br>'));
    });
}

function main() {
    $('#content').append(grid.render().el);
    ecrs.fetch({ reset: true });
    getLog();
    var socket = io.connect(window.location.protocol + '//' + window.location.host);
    socket.on('change', function () {
        ecrs.fetch({ reset: true });
    });
    socket.on('log', getLog);
    //$('#content').html("Hello from JQuery!!");
}

main();

},{}]},{},[1]);
