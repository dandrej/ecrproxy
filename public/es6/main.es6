
class Ecr extends Backbone.Model {
    parse(resp,opt) {
        _.extend(resp,{'url':{'title':resp.name, 'ref':window.location.protocol+'//'+window.location.hostname+':'+resp.port}});
        return resp;
    }
}

var Ecrs = Backbone.Collection.extend({
    model: Ecr,
    'url':"/api/ecrs"
});

class UrlCell extends Backgrid.UriCell {
    render() {
        this.$el.empty();
        var rawValue = this.model.get(this.column.get("name"));
        if (_.isObject(rawValue)) {
            this.title = rawValue.title || this.title;
            rawValue = rawValue.ref;
        }
        var formattedValue = this.formatter.fromRaw(rawValue, this.model);
        this.$el.append($("<a>", {
            tabIndex: -1,
            href: rawValue,
            title: this.title || formattedValue,
            target: this.target
        }).text(this.title || formattedValue));
        this.delegateEvents();
        return this;
    }
}

var columns = [
    {
        'name':'url',
        'label':'Каса',
        'cell':UrlCell,
        'editable':false
    },
    {
        'name':'started',
        'label':'З`єднано',
        'cell':'datetime',
        'editable':false
    }
];

var ecrs = new Ecrs();

var grid = new Backgrid.Grid({
    columns: columns,
    collection: ecrs,
    className:'backgrid table table-bordered'
});

function getLog() {
    $.get('ecrproxy.log').done(function(data){
        $('#logtxt').html(data.replace(/\r\n/,'<br>'));
    });
}

function main() {
    $('#content').append(grid.render().el);
    ecrs.fetch({reset: true});
    getLog();
    var socket = io.connect(window.location.protocol+'//'+window.location.host);
    socket.on('change', function () { ecrs.fetch({reset: true}); });
    socket.on('log', getLog);
    //$('#content').html("Hello from JQuery!!");
}

main();